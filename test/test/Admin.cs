﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace test
{
    class Admin
    {
        public static void InputDataAdmin() 
        {
            string IDAdmin = "", Nama = "", Alamat = "", x;
            bool a, z;
            a = false;
            string choice = "y";

            var dAdmin = File.ReadAllLines("DataAdmin.txt");
            Console.Write("\t\t\t\t\t\t\t Enter id (AD + 5 Angka) :");
            x = Console.ReadLine();
            foreach (string data in dAdmin)
            {
                string[] isi = data.Split(',');

                if (isi[0].Equals(x))
                {
                    a = true;
                    Console.Clear();
                    Console.WriteLine("\t\t\t\t\t\t\tId udah ada");
                    Console.ReadKey();
                    Console.Clear();
                    break;

                }
            }

            z = a;
            if (z == true)
            {
                MainMenu.Main();

            }
            while (choice == "y")
            {
                FileStream fs = new FileStream("DataAdmin.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);



                IDAdmin = x;
                Console.Write("\t\t\t\t\t\t\t Enter name :");
                Nama = Console.ReadLine();
                Console.Write("\t\t\t\t\t\t\t Enter Alamat :");
                Alamat = Console.ReadLine();

                Console.WriteLine("\t\t\t\t\t\t\t" + "ID      :" + IDAdmin + "\nNama    :" + Nama + "\nAlamat  :" + Alamat);
                Console.WriteLine("Telah Terdaftar Sebagai Admin");
                sw.Flush();
                fs.Close();
                Console.Write("\t\t\t\t\t\t\t Input Again (y/n) ?");
                choice = Console.ReadLine();
                Console.Clear();
                MainMenu.Main();
            }

        }
        
        public static void LoginAdmin()
        {
            string IDAdmin;

            string[] dbAdmin = File.ReadAllLines("DataAdmin.txt");

            Console.Write("\t\t\t\t\t\t\t ID Admin : ");
            IDAdmin = Console.ReadLine();

            foreach (string data in dbAdmin)
            {
                if (data.Contains(IDAdmin))
                {
                    var arr = data.Split(',');

                    if (arr[0].Equals(IDAdmin))
                    {
                        Console.WriteLine("\t\t\t\t\t\t\t Berhasil Login");
                        Console.ReadKey();
                    }
                    else
                    {
                        Admin.LoginAdmin();

                    }
                    break;
                }
            }
        }
        public static void ReadDataPendaftar() 
        {
            FileStream fs = new FileStream("DataAllPendaftar.txt", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            string data = sr.ReadLine();
            while (data != null) 
            {
                string[] isi = data.Split(','); 
                Console.WriteLine("\t\t\t\t\t\t\tNISN : {0}", isi[0]);
                Console.WriteLine("\t\t\t\t\t\t\tNama Lengkap : {0}", isi[1]);
                Console.WriteLine("\t\t\t\t\t\t\tAlamat : {0}", isi[2]);
                Console.WriteLine("\t\t\t\t\t\t\tTempat Tanggal Lahir : {0}", isi[3]);
                Console.WriteLine("\t\t\t\t\t\t\tAsal Sekolah : {0}", isi[4]);
                Console.WriteLine("\t\t\t\t\t\t\tPilihan Fakultas : {0}", isi[5]);
                Console.WriteLine("\t\t\t\t\t\t\tPilihan Jurusan : {0}", isi[6]);
                Console.WriteLine("\t\t\t\t\t\t\tStatus : {0}", isi[8]);
               

                data = sr.ReadLine();
                
            }
            sr.Close();
            fs.Close();
        }

        public static void SearchDataForUpdateDataFakultas() //
        {
            StringBuilder newFile = new StringBuilder(); 
            var dFakultas = File.ReadAllLines("DataFakultas.txt");
            string Fakultas;
            int pilih;
            Console.WriteLine("\t\t\t\t\t\t\tFakultas : ");
            Fakultas = Console.ReadLine();
            foreach (string data in dFakultas)
            {
                if (data.Contains(Fakultas))
                {
                    var result = data.Split('#');
                    string temp = "";
                    string NamaFakultas;
                    Console.WriteLine("\t\t\t\t\t\t\t1. update Fakultas");
                    Console.Write("\t\t\t\t\t\t\tpress 1 to update Fakultas");
                    pilih = int.Parse(Console.ReadLine());
                    switch (pilih)
                    {
                        case 1:
                            Console.WriteLine("\t\t\t\t\t\t\tOld data : {0}", result[0]);
                            Console.Write("\t\t\t\t\t\t\tEnter New Fakultas : ");
                            NamaFakultas = Console.ReadLine();
                            temp = data.Replace(result[0], NamaFakultas);
                            newFile.Append(temp + "\r\n");  
                            Console.WriteLine("\t\t\t\t\t\t\tData Telah Terganti");
                            Console.ReadKey();
                            continue;
                            break;
                        default:
                            break;
                    }

                }
                newFile.Append(data + "\r\n");
            }
            File.WriteAllText(@"DataFakultas.txt", newFile.ToString());

        }
        public static void SearchDataForUpdateJurusan() 
        {
            StringBuilder newFile = new StringBuilder(); 
            var dJurusan = File.ReadAllLines("DataJurusan.txt");
            string Jurusan;
            int pilih;
            Console.Write("\t\t\t\t\t\t\tEnter Jurusan : ");
            Jurusan = Console.ReadLine();
            foreach (string data in dJurusan)
            {
                if (data.Contains(Jurusan))
                {
                    var result = data.Split('#');
                    string temp = "";
                    string NamaJurusan;
                    Console.WriteLine("\t\t\t\t\t\t\t1. update Jurusan");
                    Console.Write("\t\t\t\t\t\t\tPress 1 to update Jurusan");
                    pilih = int.Parse(Console.ReadLine());
                    switch (pilih)
                    {
                        case 1:
                            Console.WriteLine("\t\t\t\t\t\t\tOld data : {0}", result[1]);
                            Console.WriteLine("\t\t\t\t\t\t\tEnter new Jurusan : ");
                            NamaJurusan = Console.ReadLine();
                            temp = data.Replace(result[1], NamaJurusan);
                            newFile.Append(temp + "\r\n");  
                            Console.WriteLine("\t\t\t\t\t\t\tData Telah Terganti");
                            Console.ReadKey();
                            continue;
                            break;
                        default:
                            break;
                    }

                }
                newFile.Append(data + "\r\n");
            }
            File.WriteAllText(@"DataJurusan.txt", newFile.ToString());

        }
        public static void DeleteLinesFromFileFakultas(string strLineToDelete) 
        {
            string strSearchText = strLineToDelete;
            string strOldText;
            string n = "";
            StreamReader sr = File.OpenText("DataFakultas.txt");
            while ((strOldText = sr.ReadLine()) != null)
            {
                if (!strOldText.Contains(strSearchText))
                {
                    n += strOldText + Environment.NewLine; 
                }
            }
            sr.Close();
            File.WriteAllText("DataFakultas.txt", n); 
        }

        public static void SearchDataForDeleteFakultas() 
        {
            var dFakultas = File.ReadAllLines("DataFakultas.txt");
            string Fakultas;
            string pilih = ""; 
            Console.Write("\t\t\t\t\t\t\tEnter Fakultas :");
            Fakultas = Console.ReadLine();
            foreach (string data in dFakultas)
            {
                if (data.Contains(Fakultas))
                {
                    Console.WriteLine("\t\t\t\t\t\t\tData Ditemukan : {0}", data);
                    Console.Write("\t\t\t\t\t\t\tAnda Yakin Ingin Menghapus Data? (y/n)");
                    pilih = Console.ReadLine();
                    if ((pilih == "y") || (pilih == "Y"))
                    {
                        DeleteLinesFromFileFakultas(Fakultas);
                        Console.WriteLine("\t\t\t\t\t\t\tdata berhasil di hapus");
                    }
                    else
                    {
                        Console.WriteLine("\t\t\t\t\t\t\tPress any key to continue");
                        Console.Clear();
                        //menusistem.Main();
                    }


                }
            }
        }
        public static void DeleteLinesFromFileJurusan(string strLineToDelete)  
        {
            string strSearchText = strLineToDelete;
            string strOldText;
            string j = "";
            StreamReader sr = File.OpenText("DataJurusan.txt");
            while ((strOldText = sr.ReadLine()) != null)
            {
                if (!strOldText.Contains(strSearchText))
                {
                    j += strOldText + Environment.NewLine; 
                }
            }
            sr.Close();
            File.WriteAllText("DataJurusan.txt", j); 
        }

        public static void SearchDataForDeleteJurusan()
        { 
            var dJurusan = File.ReadAllLines("DataJurusan.txt");
            string Jurusan;
            string pilih = ""; 
            Console.Write("\t\t\t\t\t\t\tkeyword :");
            Jurusan = Console.ReadLine();
            foreach (string data in dJurusan)
            {
                if (data.Contains(Jurusan))
                {
                    Console.WriteLine("\t\t\t\t\t\t\tData Ditemukan : {0}", data);
                    Console.Write("\t\t\t\t\t\t\tAnda Yakin Ingin Menghapus Data? (y/n)");
                    pilih = Console.ReadLine();
                    if ((pilih == "y") || (pilih == "Y"))
                    {
                        DeleteLinesFromFileJurusan(Jurusan);
                        Console.WriteLine("\t\t\t\t\t\t\tdata berhasil di hapus");
                    }
                    else
                    {
                        Console.WriteLine("\t\t\t\t\t\t\tPress any key to continue");
                        Console.Clear();
                        MainMenu.Main();
                    }


                }
            }
        }
      
        public static void DeleteLinesFromFilePendaftar(string strLineToDelete)  
        {
            string strSearchText = strLineToDelete;
            string strOldText;
            string p = "";
            StreamReader sr = File.OpenText("DataAllPendaftar.txt");
            while ((strOldText = sr.ReadLine()) != null)
            {
                if (!strOldText.Contains(strSearchText))
                {
                    p += strOldText + Environment.NewLine; 
                }
            }
            sr.Close();
            File.WriteAllText("DataAllPendaftar.txt", p); 
        }

        public static void SearchDataForDeletePendaftar() 
        {
            var dpendaftar = File.ReadAllLines("DataAllPendaftar.txt");
            string NISN;
            string pilih = ""; 
            Console.Write("\t\t\t\t\t\t\tEnter keyword (NISN) :");
            NISN = Console.ReadLine();
            foreach (string data in dpendaftar)
            {
                if (data.Contains(NISN))
                {
                    Console.WriteLine("\t\t\t\t\t\t\tData Ditemukan : {0}", data);
                    Console.Write("\t\t\t\t\t\t\tAnda Yakin Ingin Menghapus Data? (y/n)");
                    pilih = Console.ReadLine();
                    if ((pilih == "y") || (pilih == "Y"))
                    {
                        DeleteLinesFromFilePendaftar(NISN);
                        Console.WriteLine("\t\t\t\t\t\t\tdata berhasil di hapus");
                    }
                    else
                    {
                        Console.WriteLine("\t\t\t\t\t\t\tPress any key to continue");
                        Console.Clear();
                        MainMenu.Main();
                    }


                }
            }
        }
    }
}
