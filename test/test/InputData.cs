﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace test
{
    class InputData
    {
        public static void InputDataPendaftaranSIMAK() 
        {
            string NISN = "", NamaLengkap = "", TTL = "", Alamat = "", Fakultas = "", Jurusan = "", AsalSekolah = "", Status ="", pasword="";
            bool a;
            a = true;
            string choice = "y";

            var dPendaftar = File.ReadAllLines("DataAllPendaftar.txt");
            Console.Write("\t\t\t\t\t\t\t\t\t\tInput NISN (10 angka) :");
            NISN = Console.ReadLine();
            foreach (string data in dPendaftar)
            {
                string[] isi = data.Split(',');

                if (isi[0].Equals(NISN))
                {
                    a = false;
                    Console.WriteLine("\t\t\t\t\t\t\tId telah ada");
                    Console.ReadKey();
                    Console.Clear();

                }
                else if (NISN.Length < 10)
                {
                    Console.WriteLine("\t\t\t\t\t\tNISN Harus Terdiri Dari 10 Angka, Silakan Coba Lagi");
                    InputData.InputDataPendaftaranSIMAK();
                }
            }
            if (a == false)
            {
                MainMenu.Main();

            }
            else while (choice == "y")
            {
                FileStream fs = new FileStream("DataAllPendaftar.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);



                
                Console.Write("\t\t\t\t\t\t\tMasukan Nama Lengkap            :");
                NamaLengkap = Console.ReadLine();
                Console.Write("\t\t\t\t\t\t\tMasukan Alamat                  :");
                Alamat = Console.ReadLine();
                Console.Write("\t\t\t\t\t\t\tTempat Tanggal Lahir            :");
                TTL = Console.ReadLine();
                Console.Write("\t\t\t\t\t\t\tMasukan Asal Sekolah            :");
                AsalSekolah = Console.ReadLine();
                Console.WriteLine("\t\t\t\t\t\t\tPilihan Fakultas             :");
                InputData.ReadDataFakultas();
                Console.Write("\t\t\t\t\t\t\tInput Pilihan Fakultas           :");
                Fakultas = Console.ReadLine();
                switch (Fakultas)
                {
                    case "Fakultas Hukum":
                        Console.WriteLine("\t\t\t\t\t\t\tPilihan jurusan : ");
                        Console.WriteLine("\t\t\t\t\t\t\tIlmu Hukum");
                        break;
                    case "Fakultas Ilmu Sosial dan Ilmu Politik":
                        Console.WriteLine("\t\t\t\t\t\t\tPilihan jurusan : ");
                        Console.WriteLine("\t\t\t\t\t\t\tIlmu Politik");
                        Console.WriteLine("\t\t\t\t\t\t\tIlmu Komunikasi");
                        Console.WriteLine("\t\t\t\t\t\t\tSosiologi");
                        Console.WriteLine("\t\t\t\t\t\t\tHubungan Internasional");
                        break;
                    case "Fakultas Ilmu Pengetahuan Budaya":
                        Console.WriteLine("\t\t\t\t\t\t\tPilihan jurusan : ");
                        Console.WriteLine("\t\t\t\t\t\t\tSastra Indonesia");
                        Console.WriteLine("\t\t\t\t\t\t\tSastra Inggris");
                        Console.WriteLine("\t\t\t\t\t\t\tSastra Jepang");
                        Console.WriteLine("\t\t\t\t\t\t\tSastra Arab");
                        Console.WriteLine("\t\t\t\t\t\t\tIlmu Filsafat");
                        break;
                    case "Fakultas Teknik":
                        Console.WriteLine("\t\t\t\t\t\t\tPilihan jurusan : ");
                        Console.WriteLine("\t\t\t\t\t\t\tTeknik Metalurgi dan Material");
                        Console.WriteLine("\t\t\t\t\t\t\tTeknik Elektro");

                        break;
                    case "Fakultas Matematika dan Ilmu Pengetahuan Alam":
                        Console.WriteLine("\t\t\t\t\t\t\tPilihan jurusan : ");
                        Console.WriteLine("\t\t\t\t\t\t\tMatematika");
                        Console.WriteLine("\t\t\t\t\t\t\tKimia");
                        Console.WriteLine("\t\t\t\t\t\t\tBiologi");
                        Console.WriteLine("\t\t\t\t\t\t\tFisika");
                        Console.WriteLine("\t\t\t\t\t\t\tGeografi");
                        break;
                    case "Fakultas Ekonomi":
                        Console.WriteLine("\t\t\t\t\t\t\tPilihan jurusan : ");
                        Console.WriteLine("\t\t\t\t\t\t\tManajemen");
                        Console.WriteLine("\t\t\t\t\t\t\tIlmu Ekonomi");
                        Console.WriteLine("\t\t\t\t\t\t\tAkutansi");

                        break;

                }
                    //break;
                Console.Write("\t\t\t\t\t\t\tInput Jurusan                 :");
                Jurusan = Console.ReadLine();
                Console.Write("\t\t\t\t\t\t\tPassword                      : ");
                pasword = Console.ReadLine();


              
                
                sw.WriteLine(NISN + "," + NamaLengkap + "," + Alamat + "," + TTL + "," + AsalSekolah + "," + Fakultas + "," + Jurusan + "," + Status + pasword + "," + "BB");
                Console.Clear();
                Console.WriteLine(NISN + "," + NamaLengkap + "," + Alamat + "," + TTL + "," + AsalSekolah + "," + Fakultas + "," + Jurusan + "," + Status + pasword + "," + "BB");

                Console.WriteLine("\t\t\t\t\t\t===================================================================");
                Console.WriteLine("\t\t\t\t\t\tSilakan Melakukan Pembayaran sebesar Rp. 350.000,-");
                Console.WriteLine("\t\t\t\t\t\tSetelah Melakukan Pembayaran Silakan Login dan melakukan konfirmasi");
                Console.WriteLine("\t\t\t\t\t\tBatas Waktu Pembayaran 5 Hari Setelah Anda Mendaftar");
                sw.Flush();
                
                fs.Close();
                Console.Write("\t\t\t\t\t\tInput Again Y/N :");
                choice = Console.ReadLine();
            

            MainMenu.Main();
        }

    }
        


        public static void InputDataPembayaran() 
        {
            string NISN, TOTAL, TanggaLPembayaran;


            string choice = "y";
            while (choice == "y")
            {
                Console.Clear();
                var dbPendaftar = File.ReadAllLines("DataAllPendaftar.txt");
                //bool chkID = false;

                Console.Write("\t\t\t\t\t\tInput NISN :");
                NISN = Console.ReadLine();
                
                foreach (string data in dbPendaftar)
                {
                    string[] isi = data.Split(',');

                    if (isi[0].Equals(NISN))
                    {                        
                        Console.WriteLine("\t\t\t\t\t\t NISN                        : {0}", isi[0]);
                        Console.WriteLine("\t\t\t\t\t\t Nama Lengkap                : {0}", isi[1]);
                        Console.WriteLine("\t\t\t\t\t\t Alamat                      : {0}", isi[2]);
                        Console.WriteLine("\t\t\t\t\t\t Tempat tanggal lahir        : {0}", isi[3]);
                        Console.WriteLine("\t\t\t\t\t\t Fakultas Pilihan            : {0}", isi[5]);
                        Console.WriteLine("\t\t\t\t\t\t Jurusan Pilihan             : {0}", isi[6]);
                        Console.WriteLine("\t\t\t\t\t\tStatus pembayaran            : {0}", isi[8]);  
                    }
                }
             

                    FileStream fs = new FileStream("DataAllPendaftar.txt", FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                 
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                    Console.Write("\t\t\t\t\t\t Back To MainMenu (y/n) :");
                    choice = Console.ReadLine();
                    MainMenu.Main();
                    break;
                
            }
           
        }
        public static void EnterFakultas()  
        {
            string Fakultas = "";
            string choice = "y"; 
            while (choice == "y")
            {
                FileStream fs = new FileStream("DataFakultas.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);

               
                Console.WriteLine("\t\t\t\t\t\tInput Fakultas : ");
                Fakultas = Console.ReadLine();

                sw.WriteLine(Fakultas);
                sw.Flush();
                sw.Close();
                fs.Close();
                Console.Write("\t\t\t\t\t\tinput again(y/n)");
                choice = Console.ReadLine();
            }
        }

        public static void ReadDataFakultas() 
        {
            FileStream fs = new FileStream("DataFakultas.txt", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            string data = sr.ReadLine();
            while (data != null) 
            {
                string[] isi = data.Split('#'); 
                Console.WriteLine("\t\t\t\t\t\tFakultas : {0}", isi[0]);

                data = sr.ReadLine();
               
            }
            sr.Close();
            fs.Close();
        }
        public static void EnterJurusan()  
        {
            string ID = "", Jurusan = "";
            string choice = "y"; 
            while (choice == "y")
            {
                FileStream fs = new FileStream("DataJurusan.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                Console.Write("\t\t\t\t\t\tInput ID Jurusan  : ");
                ID = Console.ReadLine();
                Console.Write("\t\t\t\t\t\tInput Jurusan     : ");
                Jurusan = Console.ReadLine();

                sw.WriteLine("\t\t\t\t\t\t" + ID + "#" + Jurusan);
                Console.WriteLine("\t\t\t\t\t\t" + ID + "#" + Jurusan);
                sw.Flush();
                sw.Close();
                fs.Close();
                Console.Write("\t\t\t\t\t\tinput again(y/n) :");
                choice = Console.ReadLine();
            }
        }

        public static void ReadDataJurusan() 
        {
            FileStream fs = new FileStream("datajurusan.txt", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            string data = sr.ReadLine();
            while (data != null) 
            {
                string[] isi = data.Split('#'); 
                Console.WriteLine("\t\t\t\t\t\tID : {0}", isi[0]);
                Console.WriteLine("\t\t\t\t\t\tJurusan : {0}", isi[1]);

                data = sr.ReadLine();
                
            }
            sr.Close();
            fs.Close();
        }
    }
}