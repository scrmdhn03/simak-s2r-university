﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace test
{
    class Login
    {
        public static void LoginPendaftar()
        {
            string NISN, Pasword;

            string[] dbPendaftar = File.ReadAllLines("DataAllPendaftar.txt");

            Console.Write("\t\t\t\t\t\t\t\t\t\t\t NISN : ");
            NISN = Console.ReadLine();
            Console.Write("\t\t\t\t\t\t\t Password : ");
            Pasword = Console.ReadLine();

            foreach (string data in dbPendaftar)
            {
                if (data.Contains(NISN))
                {
                    var arr = data.Split(',');

                    if (arr[0].Equals(NISN))
                    {
                        if (arr[7].Equals(Pasword))
                        {
                            Console.WriteLine("\t\t\t\t\t\t\t Berhasil Login");
                            Console.ReadKey();
                            break;
                        }
                        else
                        {
                            Login.LoginPendaftar();
                            break;
                        }
                      
                    }
                    else
                    {
                        Login.LoginPendaftar();

                    }
                    break;
                }
            }
        }
        public static void PrintKartu()
        {
            string NISN;
            int tekan;
            var dDaftar = File.ReadAllLines("DataAllPendaftar.txt");

            Console.WriteLine("\t\t\t\t\t\t\t Input NISN :");
            NISN = Console.ReadLine();
            foreach (string data in dDaftar)
            {
                if (data.Contains(NISN))
                {
                    var arr = data.Split(','); 

                    if (arr[0].Equals(NISN))
                    {
                        if (arr[8].Equals("BB"))
                        {
                            Console.WriteLine("\t\t\t\t\t\t\t Silakan Lakukan Pembayaran terlebih dahulu ");
                            Console.ReadKey();
                            break;
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\t\t\t\t\t\t\t Kartu Tanda Peserta Ujian");
                            Console.WriteLine("\t\t\t\t\t\t\t============================================");
                            Console.WriteLine("\t\t\t\t\t\t\t============================================");
                            Console.WriteLine("\t\t\t\t\t\tNISN                 : {0}", arr[0]);
                            Console.WriteLine("\t\t\t\t\t\tNama Lengkap         : {0}", arr[1]);
                            Console.WriteLine("\t\t\t\t\t\tAlamat               : {0}", arr[2]);
                            Console.WriteLine("\t\t\t\t\t\tTempat tanggal lahir : {0}", arr[3]);
                            Console.WriteLine("\t\t\t\t\t\tAsal Sekolah         : {0}", arr[4]);
                            Console.WriteLine("\t\t\t\t\t\tFakultas             : {0}", arr[5]);
                            Console.WriteLine("\t\t\t\t\t\tJurusan              : {0}", arr[6]);
                            Console.WriteLine("\t\t\t\t\t\t\t---------------------------------------------");
                            Console.WriteLine("\t\t\t\t\t\t\tUjian Dilaksanakan Pada :");
                            Console.WriteLine("\t\t\t\t\t\t\tTanggal  : 5 April 2018");
                            Console.WriteLine("\t\t\t\t\t\t\tLokasi   : Balairung S2R University");
                            Console.WriteLine("\t\t\t\t\t\t\tWaktu    : 09.00 - 11.00 ");
                            Console.WriteLine("\n\n\n\t\t\t\t\t\t Harap Membawa Peralatan Ujian, Bukti Pembayaran serta Kartu Identitas");
                            Console.ReadKey();
                            break;
                        }
                    }

                    else
                    {
                        Login.PrintKartu();
                    }
                    break;
                }
            }
        }
    }
}
