﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class MainMenu
    {
        public static void Main()
        {
            Console.Clear(); 
            int Choice;
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\t\t\t\t\t\t================================================");
            Console.WriteLine("\t\t\t\t\t\t=========== Tes Masuk S2R University ===========");
            Console.WriteLine("\t\t\t\t\t\t================================================");
            Console.WriteLine("\t\t\t\t\t\t");
            

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\t\t\t\t\t\t================================================");
            Console.WriteLine("\t\t\t\t\t\t\t\t\tMenu");
            Console.WriteLine("\t\t\t\t\t\t================================================");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\t\t\t\t\t\t\t 1. Menu Admin");
            Console.WriteLine("\t\t\t\t\t\t\t 2. Menu Pendaftar");
            Console.Write("\t\t\t\t\t\t\t Please Choose Your Choice :");
            Choice = Convert.ToInt32(Console.ReadLine());

            switch (Choice)
            {
                case 1:
                    int pChoice;
                    Console.WriteLine("\t\t\t\t\t\t================================================");
                    Console.WriteLine("\t\t\t\t\t\t\t\t\tMenu");
                    Console.WriteLine("\t\t\t\t\t\t================================================");
                    Console.WriteLine("\t\t\t\t\t\t\t1. Login Admin");
                    Console.Write("\t\t\t\t\t\t\t Choose 1 :");
                    pChoice = Convert.ToInt16(Console.ReadLine());

                    switch (pChoice)
                    {
                        case 1:
                            int tpilih;
                            Admin.LoginAdmin();
                            Console.WriteLine("\t\t\t\t\t\t\t1.Input Admin Baru");
                            Console.WriteLine("\t\t\t\t\t\t\t2.Read Data Pendaftar");
                            Console.WriteLine("\t\t\t\t\t\t\t3.Read Data Fakultas");
                            Console.WriteLine("\t\t\t\t\t\t\t4.Read Data Jurusan");
                            Console.WriteLine("\t\t\t\t\t\t\t5.Update Data Fakultas");
                            Console.WriteLine("\t\t\t\t\t\t\t6.Update Data Jurusan");
                            Console.WriteLine("\t\t\t\t\t\t\t7.Update Data Pembayaran");
                            Console.WriteLine("\t\t\t\t\t\t\t8.Delete Data Fakultas");
                            Console.WriteLine("\t\t\t\t\t\t\t9.Delete Data Jurusan");
                            Console.Write("\t\t\t\t\t\t\t choose your choice :");
                            tpilih = Convert.ToInt16(Console.ReadLine());
                            switch (tpilih)
                            {
                                case 1:
                                    Admin.InputDataAdmin();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue");
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 2:
                                    Admin.ReadDataPendaftar();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 3:
                                    InputData.ReadDataFakultas();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 4:
                                    InputData.ReadDataJurusan();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 5:
                                    
                                    Admin.SearchDataForUpdateDataFakultas();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 6:
                                    
                                    Admin.SearchDataForUpdateJurusan();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 7:

                                    TransaksiPembayaran.SearchDataForUpdatePembayaran();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();

                                    break;
                                case 8:
                                    
                                    Admin.SearchDataForDeleteFakultas();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue");
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                case 9:
                                    
                                    Admin.SearchDataForDeleteJurusan();
                                    Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                                    Console.ReadKey();
                                    Console.Clear();
                                    MainMenu.Main();
                                    break;
                                default:
                                    break;
                            }
                            break;


                    }
                    break;
                    MainMenu.Main();

                case 2:

                    Console.WriteLine("\t\t\t\t\t\t================================================");
                    Console.WriteLine("\t\t\t\t\t\t\tMenu Pendaftar");
                    Console.WriteLine("\t\t\t\t\t\t================================================");
                    Console.WriteLine("\t\t\t\t\t\t\t   1.Daftar Baru ");
                    Console.WriteLine("\t\t\t\t\t\t\t   2.Login pendaftar");
                    Console.Write("\t\t\t\t\t\t\t Choose your choise :");
                    pChoice = Convert.ToInt16(Console.ReadLine());
                    Console.Write("\t\t\t\t\t\t\t______________________________ ");
                    

                    switch(pChoice)
                    {
                        case 1:
                            InputData.InputDataPendaftaranSIMAK();
                            Console.WriteLine("\t\t\t\t\t\t\tpress any key to continue"); 
                            Console.ReadKey();
                            Console.Clear();
                           
                            break;
                        case 2:
                            int pLogin;
                            Login.LoginPendaftar();
                            Console.WriteLine("\t\t\t\t\t\t\t 1. Konfirmasi Pembayaran");
                            Console.WriteLine("\t\t\t\t\t\t\t 2. Print Kartu Peserta Ujian :");
                            Console.Write("\t\t\t\t\t Tekan 1 Untuk konfirmasi pembayaram, Tekan 2 Untuk Print Kartu Ujian :");

                            pLogin = Convert.ToInt16(Console.ReadLine());
                            switch (pLogin)
                            {
                                case 1:
                                    InputData.InputDataPembayaran();
                                    break;
                                case 2:
                                    Login.PrintKartu();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                    Console.ReadKey();
                    MainMenu.Main();

            }
            
        }

    }
}
